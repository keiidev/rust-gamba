import React from "react";

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

const Button: React.FC<ButtonProps> = ({ children, ...props }) => {
  return (
    <button
      {...props}
      className="rounded-md bg-blue-500 hover:bg-blue-600 active:bg-blue-400 focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 duration-75 px-12 py-2 font-semibold text-white"
    >
      {children}
    </button>
  );
};

export default Button;
