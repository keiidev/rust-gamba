import Image from "next/image";
import { Inter } from "next/font/google";
import Button from "@/components/Button";
import { useEffect, useRef, useState } from "react";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  let [angle, setAngle] = useState<number>();

  return (
    <main className="p-4">
      <Button onClick={() => setAngle(Math.random() * 360)}>Gamba</Button>
      <img
        src="/wheel.png"
        style={{ rotate: `${angle}deg` }}
        className="mt-9 transition-all duration-[10000ms]"
      />
    </main>
  );
}
